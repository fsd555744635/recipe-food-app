// addrecipes.component.ts
import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../recipe.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addrecipes',
  templateUrl: './addrecipes.component.html',
  styleUrls: ['./addrecipes.component.css']
})
export class AddrecipesComponent implements OnInit {
  recipes: any = { title: '', description: '', category: '', chefName: '', duration: 0, image: null };

  constructor(private recipeService: RecipeService, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.recipeService.addRecipes(
      this.recipes.image,
      this.recipes.title,
      this.recipes.description,
      this.recipes.category,
      this.recipes.chefName,
      this.recipes.duration
    ).subscribe(
      (recipe: any) => {
        console.log('Recipe added successfully:', recipe);
        this.toastr.success('Recipe added successfully', 'Success');
        // Clear form fields after successful submission
        this.recipes.title = '';
        this.recipes.description = '';
        this.recipes.category = '';
        this.recipes.chefName = '';
        this.recipes.duration = 0;
        this.recipes.image = null;
      },
      (error: any) => {
        console.error('Error adding recipe:', error);
        this.toastr.error('Error adding recipe', 'Error');
      }
    );
  }

  onFileChange(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.recipes.image = fileList[0];
    }
  }
}
