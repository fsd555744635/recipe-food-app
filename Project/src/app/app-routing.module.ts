import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RecipesComponent } from './recipes/recipes.component';
import { VegItemComponent } from './vegitem/vegitem.component';
import { NonVegItemComponent } from './non-vegitem/non-vegitem.component';
import { authGuard } from './auth.guard';
import { CartComponent } from './cart/cart.component';
import { AboutComponent } from './about/about.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { OtpComponent } from './otp/otp.component';
import { PaymentComponent } from './payment/payment.component';
import { CustomersComponent } from './customers/customers.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchcustomersComponent } from './searchcustomers/searchcustomers.component';
import { AddrecipesComponent } from './addrecipes/addrecipes.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { OrdernowComponent } from './ordernow/ordernow.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'recipes', canActivate: [authGuard], component: RecipesComponent },
  { path: 'addrecipes', canActivate: [authGuard], component: AddrecipesComponent },
  { path: 'customers', component: CustomersComponent, canActivate: [authGuard], },
  { path: 'otp', canActivate: [authGuard], component: OtpComponent },
  { path: 'admin', canActivate: [authGuard], component: AdminComponent },
  { path: 'cart', component: CartComponent, canActivate: [authGuard] },
  { path: 'non-vegitem', component: NonVegItemComponent, canActivate: [authGuard] },
  { path: 'vegitem', component: VegItemComponent, canActivate: [authGuard] },
  { path: 'payment', component: PaymentComponent },
  { path: 'ingredients', component: IngredientsComponent, canActivate: [authGuard] },
  { path: 'searchCustomers', canActivate: [authGuard], component: SearchcustomersComponent },
  { path: 'logout', component: LogoutComponent},
  { path: 'ordernow',canActivate: [authGuard], component: OrdernowComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
