import { BrowserModule} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { GenderPipe } from './gender.pipe';
import { CartComponent } from './cart/cart.component';
import { RecipesComponent } from './recipes/recipes.component';
import { UserComponent } from './user/user.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VegItemComponent } from './vegitem/vegitem.component';
import { NonVegItemComponent } from './non-vegitem/non-vegitem.component';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { AdminComponent } from './admin/admin.component';
import { AddrecipesComponent } from './addrecipes/addrecipes.component';
import { MyrecipesComponent } from './myrecipes/myrecipes.component';
import { OtpComponent } from './otp/otp.component';
import { PaymentComponent } from './payment/payment.component';
import { ProfileComponent } from './profile/profile.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CustomersComponent } from './customers/customers.component';
import { SearchcustomersComponent } from './searchcustomers/searchcustomers.component';
import { CustomerService } from './customer.service';
import { RecipeService } from './recipe.service';
import { DropdownComponent } from './dropdown/dropdown.component';
import { CartService } from './cart.service';
import { OrdernowComponent } from './ordernow/ordernow.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    HeaderComponent,
    GenderPipe,
    CartComponent,
    RecipesComponent,
    UserComponent,
    IngredientsComponent,
    HomeComponent,
    AboutComponent,
    VegItemComponent,
    NonVegItemComponent,
    FooterComponent,
    AdminComponent,
    AddrecipesComponent,
    MyrecipesComponent,
    OtpComponent,
    PaymentComponent,
    ProfileComponent,
    WelcomeComponent,
    CustomersComponent,
    SearchcustomersComponent,
    DropdownComponent,
    OrdernowComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    ToastrModule.forRoot(), 
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgxCaptchaModule
  ],
  providers: [
    CustomerService,
    RecipeService,
    CartService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }