// cart.service.ts

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartItems: any;
  private cartItemsSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  cartItems$: Observable<any[]> = this.cartItemsSubject.asObservable();
  totalAmount: number = 0;

  constructor(private http: HttpClient) {
    this.cartItems = [];
  }

  addToCart(item: any) {
    const currentItems = this.cartItemsSubject.getValue();
    const updatedItems = [...currentItems, { ...item, quantity: 1 }];
    this.cartItemsSubject.next(updatedItems);
  }

  removeCartItem(item: any) {
    const currentItems = this.cartItemsSubject.getValue();
    const updatedItems = currentItems.filter(cartItem => cartItem.id !== item.id);
    this.cartItemsSubject.next(updatedItems);
  }

  emptyCart() {
    this.cartItemsSubject.next([]);
  }

  updateTotalAmount() {
    this.cartItems$.subscribe(items => {
      const totalAmount = items.reduce((sum: number, item: any) => {
        const itemPrice = item.price || 0;
        const itemQuantity = item.quantity || 1;
        return sum + (itemPrice * itemQuantity);
      }, 0);

      this.totalAmount = totalAmount;
      console.log('Updated total amount:', totalAmount);
    });
  }
}
