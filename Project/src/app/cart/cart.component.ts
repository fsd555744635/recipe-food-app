// cart.component.ts

import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartItems: any[] = [];
  total: number = 0;
  cdr: any;
  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.cartService.cartItems$.subscribe(items => {
      this.cartItems = items;
      this.calculateTotal();
    });
  }

  calculateTotal() {
    this.total = this.cartItems.reduce((sum, item) => {
      const itemPrice = item.price || 0;
      const itemQuantity = item.quantity || 1;
      return sum + itemPrice * itemQuantity;
    }, 0);
    console.log('Calculated total:', this.total);
    this.detectChanges();
  }

  getTotalPrice(): number {
    return this.total;
  }

  removeItem(item: any) {
    const index = this.cartItems.findIndex(cartItem => this.isEqual(cartItem, item));
    if (index !== -1) {
      this.cartItems.splice(index, 1);
      this.calculateTotal();
    }
  }


  emptyCart() {
    this.cartService.emptyCart();
  }

  get totalAmount() {
    return this.cartItems.reduce((total, item) => total + item.quantity * item.price, 0);
  }

  private isEqual(arr1: any[], arr2: any[]): boolean {
    if (arr1.length !== arr2.length) {
      return false;
    }

    for (let i = 0; i < arr1.length; i++) {
      // Check if each item is equal based on a unique identifier (e.g., id)
      if (arr1[i].id !== arr2[i].id) {
        return false;
      }
    }

    return true;
  }

  private detectChanges() {
    // Angular's default change detection strategy will handle updates to the view
    // No additional logic is needed here in a simple case like this
    this.cdr.detectChanges();
  }

}
