import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {


  updateCart(recipes: any) {
    throw new Error('Method not implemented.');
  }
  cartRecipes: any;
  loggedInUserEmail: any;

  getIsUserLogged() {
    throw new Error('Method not implemented.');
  }

  isUserLoggedIn: boolean;
  loginStatus: any;
  // cartRecipes : any;
  customer: any;
  otp: number;

  constructor(private http: HttpClient) {
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
    // this.cartpackages=[];
    this.otp = 0;
    this.customer = {
      customerName: '',
      email: '',
      gender: '',
      phoneNumber: '',
      password: '',
      confirmPassword: ''
    };
  }

  sendOtpToCustomer(phoneNumber: string, otp: number): Observable<any> {
    const otpData = { phoneNumber, otp };
    return this.http.post(`http://localhost:8080/sendOtp/${phoneNumber}/${otp}`, {});
  }

  registerCustomer(): any {
    return this.http.post('http://localhost:8080/addCustomer', this.customer);
  }

  customerLogin(email: any, password: any): any {
    return this.http.get('http://localhost:8080/customerLogin/' + email + '/' + password)
      .toPromise()
      .catch((error: any) => {
        console.error('Error during login:', error);
        throw error;  // Rethrow the error to propagate it to the component
      });
  }


  sendMail(mail: string, message: string) {
    return this.http.get('http://localhost:8080/sendMail/' + mail + '/' + message);
  }

  getAllCustomers(): any {
    return this.http.get('http://localhost:8080/getCustomers');
  }

  getCustomerById(customerId: any): any {
    return this.http.get('http://localhost:8080/getCustomerById/' + customerId)
  }

  updateCustomerProfile(customerData: any): Observable<any> {
    return this.http.put('http://localhost:8080/updateCustomer', customerData);
  }

  deleteCustomer(customerId: any): Observable<any> {
    return this.http.delete(`http://localhost:8080/deleteCustomer/${customerId}`);
  }

  updateCustomer(updatedCustomer: any): Observable<any> {
    return this.http.put('http://localhost:8080/updateCustomer', updatedCustomer);
  }

  getLoggedInUser(): Observable<any> {
    console.log('Fetching logged-in user data for email:', this.loggedInUserEmail);
    return this.http.get<any>('http://localhost:8080/getCustomerByEmail/' + this.loggedInUserEmail);
  }

  getCartItems(): Observable<any> {
    // Adjust the URL according to your server endpoint
    return this.http.get<any>('http://localhost:8080/getCartItems');
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  getIsUserLoggedIn(): boolean {
    return this.isUserLoggedIn;
  }

  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }
};