import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrl: './customers.component.css'
})
export class CustomersComponent implements OnInit{

  customers : any;

  constructor(private service : CustomerService){ }

  ngOnInit() {
    this.fetchCustomers();
  }

  fetchCustomers() {
    this.service.getAllCustomers().subscribe((data: any) => {
      this.customers = data;
    });
  }

  deleteCustomer(customerId: any) {
    this.service.deleteCustomer(customerId).subscribe(
      (data: any) => {
        console.log('Customer deleted successfully');
        // Refresh the customer list
        this.fetchCustomers();
      },
      (error: any) => {
        console.error('Error deleting customer:', error);
      }
    );
  }

  openUpdateModal(customer: any) {
    console.log('Open update modal for customer:', customer);
  }

  updateCustomer(updatedCustomer: any) {
    this.service.updateCustomer(updatedCustomer).subscribe(
      (data: any) => {
        console.log('Customer updated successfully');
        // Refresh the customer list
        this.fetchCustomers();
      },
      (error: any) => {
        console.error('Error updating customer:', error);
      }
    );
  }
}