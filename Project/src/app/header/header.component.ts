import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
  loginstatus: any;
  cartItems: any;

  constructor(private service: CustomerService) {
    this.cartItems = service.getCartItems();
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginstatus = data;
    });
  }
}
