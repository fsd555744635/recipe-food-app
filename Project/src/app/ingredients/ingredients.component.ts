// import { Component } from '@angular/core';
// import { Router } from 'express';
// import { CartService } from '../cart.service';

// @Component({
//   selector: 'app-ingredients',
//   templateUrl:'./ingredients.component.html',
//   styleUrl: './ingredients.component.css'
// })
// export class IngredientsComponent{
//   constructor(private router: Router, private cartService: CartService) { }

//   ingredients = [
//     { name: 'Briyani Masala', image: '/assets/Images/briyaniMasala.jpg', description: 'A special blend of spices to enhance the flavor of your biryani.', price: 50.00 },
//     { name: 'Chicken Masala', image: '/assets/Images/aachiChickenMasala.jpg', description: 'Perfectly crafted masala for delicious chicken preparations.', price: 60.00 },
//     { name: 'Paneer Masala', image: '/assets/Images/paneerMasala.jpg', description: 'Authentic masala to create mouth-watering paneer dishes.', price: 40.00 },
//     { name: 'Turmeric Powder', image: '/assets/Images/haldi.jpg', description: 'High-quality turmeric powder for its vibrant color and health benefits.', price: 20.00 },
//     { name: 'Mirchi Powder', image: '/assets/Images/mirchipowder.jpg', description: 'Spicy red chili powder to add heat to your dishes.', price: 50.00 },
//     { name: 'Cooking Oil', image: '/assets/Images/oil.jpg', description: 'Pure and versatile cooking oil for all your culinary needs.', price: 120.00 },
//     { name: 'Salt', image: '/assets/Images/salt.jpg', description: 'Premium quality salt to enhance the taste of your dishes.', price: 20.00 },
//     { name: 'Mutton Masala', image: '/assets/Images/muttonMasala.jpg', description: 'Specially blended masala for flavorful mutton preparations.', price: 60.00 },
//     { name: 'Egg Masala', image: '/assets/Images/eggMasala.jpg', description: 'Exquisite masala to make your egg dishes more delightful.', price: 30.00 },
//     { name: 'Garam Masala', image: '/assets/Images/garamMasala.jpg', description: 'Aromatic garam masala for a rich and warming flavor.', price: 20.00 },
//     { name: 'Fish Masala', image: '/assets/Images/fishMasala.jpg', description: 'Delicious masala perfect for seasoning your fish recipes.', price: 55.00 },
//     { name: 'Pav Bhaji Masala', image: '/assets/Images/pavBhajiMasala.jpg', description: 'Authentic spice blend for making the popular pav bhaji dish.', price: 35.00 },
//     { name: 'Full Masala Set', image: '/assets/Images/fullMasalaSet.jpg', description: 'Complete set of masalas for a variety of culinary delights.', price: 50.00 },
//     { name: 'Masala Box', image: '/assets/Images/MasalaBox.jpg', description: 'Organized masala box with a variety of essential spices.', price: 60.00 },
//   ];

// showSuccessMessage: boolean = false;

// addToCart(ingredient: any) {
//   this.cartService.addToCart(ingredient);

//   this.showSuccessMessage = true;
//   setTimeout(() => {
//     this.showSuccessMessage = false;
//   }, 3000);
// }
// }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ingredients',
  templateUrl:'./ingredients.component.html',
  styleUrl: './ingredients.component.css'
})
export class IngredientsComponent implements OnInit {

  ingredients: any[] = [];
  newIngredient: any = { name: '' }; 
  // newIngredient: any = {};

  constructor() {}

  ngOnInit(): void {
  }

  }

