declare var google: any;
declare var grecaptcha: any;
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  showFooter: boolean = false;
  captcha: string = '';
  captchaInput: string = '';
  isCaptchaValid: boolean = false;
  captchaResolved: boolean = false;
  siteKey: string = "6LeJjWgpAAAAALvszYULjRzlg7eUUo_49Q_wsHnO";
  showRegistrationForm: boolean = false;
  email: any;
  loginStatus: any;
  loginForm: NgForm;
  showPassword: boolean = false;
  protected aFormGroup: FormGroup;
  customer: any;

  constructor(private service: CustomerService, private router: Router, private toastr: ToastrService, private formBuilder: FormBuilder) {
    this.loginForm = {} as NgForm;
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['']
    });
  }

  onCaptchaResolved(event: any) {
    this.isCaptchaValid = true;
    this.captchaResolved = true;
    
    grecaptcha.execute(this.siteKey, { action: 'login' }).then((token: string) => {
    });
  }

  ngOnInit() {
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['']
    });

    grecaptcha.ready(() => {
      grecaptcha.execute(this.siteKey, { action: 'login' }).then((token: string) => {
      });
    });

    google.accounts.id.initialize({
      client_id: '317949943993-jkvtflfkkvnak55c7phm0pnlmpgvb6v4.apps.googleusercontent.com',
      callback: (resp: any) => this.handleLogin(resp)
    });

    google.accounts.id.renderButton(document.getElementById("google-btn"), {
      theme: 'filled_blue',
      size: 'large',
      shape: 'rectangle',
      width: 350
    });
  }

  private decodeToken(token: string) {
    return JSON.parse(atob(token.split(".")[1]));
  }

  handleLogin(response: any) {
    console.log("working");
    if (response) {
      const payLoad = this.decodeToken(response.credential);
      sessionStorage.setItem("loggedInUser", JSON.stringify(payLoad));
      localStorage.setItem("email", payLoad.email);
      this.service.setIsUserLoggedIn();
      this.router.navigate(['recipes']);
    }
  }

  async loginSubmit(formData: any, form: NgForm) {
    if (!formData.email || !formData.password) {
      this.toastr.error('Both email and password are required');
      return;
    }

    console.log(formData.email);
    console.log(formData.password);

    if (formData.email == 'admin@gmail.com' && formData.password == 'Admin@123') {
      this.service.setIsUserLoggedIn();
      localStorage.setItem('email', formData.email);
      this.router.navigate(['admin']);
    } else {
      this.customer = null;
    }

    await this.service.customerLogin(formData.email, formData.password)
      .then((data: any) => {
        console.log(data);
        this.customer = data;

        if (this.customer != null) {
          localStorage.setItem("customerid", this.customer.customerId);
          localStorage.setItem("email", this.customer.email);

          this.service.setIsUserLoggedIn();
          this.toastr.success("Customer login Successful!!");
          this.router.navigate(['recipes']);
        } else {
          this.toastr.error("Invalid Login credentials");
        }
      })
      .catch((error: any) => {
        console.error('Login failed:', error);
        // Handle the error or show a relevant message to the user
        this.toastr.error("Login failed. Please try again.");
      });
  }

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }
}
