import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-non-vegitem',
  templateUrl: './non-vegitem.component.html',
  styleUrls: ['./non-vegitem.component.css']
})
export class NonVegItemComponent {
  nonVegItems: any[] = [];
  cartRecipes: any[];
  
  showSuccessMessage: boolean = false;

  constructor(private router: Router, private cartService: CartService) {
    this.cartRecipes = [];
    
    this.nonVegItems = [
      { name: 'Chicken Gravy', image: '/assets/Images/Chicken.jpg', description: 'Intense & Concentrated', price: 120.00 },
      { name: 'Mutton Gravy', image: '/assets/Images/Mutton.jpg', description: 'Rich & Frothy', price: 160.00 },
      { name: 'Mutton Liver', image: '/assets/Images/mutton liver.jpg', description: 'Creamy & Whipped', price: 80.00 },
      { name: 'Prawans', image: '/assets/Images/prawans .jpg', description: 'Blended & Refreshing', price: 100.00 },
      { name: 'Boiled Egg', image: '/assets/Images/eggs1.jpg', description: 'Chilled & Delightful', price: 20.00 },
      { name: 'Egg Gravy', image: '/assets/Images/Egg.jpg', description: 'Vanilla & Caramel', price: 50.00 },
      { name: 'Chicken Briyani', image: '/assets/Images/Chicekn Briyani.jpg', description: 'Smooth & Bold', price: 120.00 },
      { name: 'Mutton Briyani', image: '/assets/Images/mutton Briyanijpg.jpg', description: 'Classic & Perfected', price: 200.00 },
      { name: 'Egg Briyani', image: '/assets/Images/Egg briyani .jpg', description: 'Satisfying & Wholesome', price: 100.00 },
      { name: 'Mutton Bone Soup', image: '/assets/Images/Bone Soup.jpg', description: 'Warm & Nourishing', price: 100.00 },
    ];
  }

  addToCart(nonveg: any) {
    this.cartService.addToCart(nonveg); // Use the common addToCart method
    this.showSuccessMessage = true;
    setTimeout(() => {
      this.showSuccessMessage = false;
    }, 3000);
  }
}