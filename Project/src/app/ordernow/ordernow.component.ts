declare var Razorpay: any;

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-ordernow',
  templateUrl: './ordernow.component.html',
  styleUrls: ['./ordernow.component.css']
})
export class OrdernowComponent implements OnInit {

  custEmail: any;
  totalAmount: any;
  cartItems: any;
  recipes: any;
  cartrecipes: any[] = [];
  showPaymentDetails: boolean = false;
  paymentDetails: any = {
    name: '',
    email: '',
    phone: ''
  };

  constructor(private recipeService: RecipeService, private cartService: CartService) {
    this.cartItems = [];
    this.totalAmount = 0;
  }

  ngOnInit() {
    this.recipeService.cartItems$.subscribe((cartItems: any) => {
      this.cartItems = cartItems;
      this.calculateTotal();
    });
  }

  calculateTotal(): void {
    this.totalAmount = this.cartItems.reduce((sum, item) => {
      const itemPrice = item.price || 0;
      const itemQuantity = item.quantity || 1;
      return sum + (itemPrice * itemQuantity);
    }, 0);
  }

  loadRazorpay(): void {
    const script = document.createElement('script');
    script.src = 'https://checkout.razorpay.com/v1/checkout.js';
    script.async = true;
    script.onload = () => {
      console.log('Razorpay script loaded');
    };
    document.head.appendChild(script);
  }

  openPaymentForm(): void {
    this.showPaymentDetails = true;
  }

  PayNow(): void {
    const RazorpayOptions = {
      description: 'Food Recipe Order Payment',
      currency: 'INR',
      amount: this.totalAmount * 100,
      name: 'RecipeRadar',
      key: 'rzp_test_4PkJtjg71JE6ux', 
      prefill: {
        name: this.paymentDetails.name,
        email: this.paymentDetails.email,
        contact: this.paymentDetails.phone
      },
      theme: {
        color: '#6466e3'
      },
      modal: {
        ondismiss: () => {
          console.log('Payment dismissed');
        }
      }
    };

    const successCallback = (paymentid: any) => {
      console.log(paymentid);
      // Add logic here for successful payment
    };

    const failureCallback = (error: any) => {
      console.error(error);
      // Add logic here for payment failure
    };

    if (typeof Razorpay !== 'undefined') {
      Razorpay.open(RazorpayOptions, successCallback, failureCallback);
    } else {
      console.error('Razorpay has not been loaded yet.');
    }
  }
}