import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.css'
})
export class ProfileComponent {
  customerData: any = {};
  constructor(private service: CustomerService, private toastr: ToastrService) { }

  ngOnInit(): void {
    // Fetch user data when the component initializes
    this.fetchCustomerData();
    // Fetch countries
  }
  fetchCustomerData() {
    throw new Error('Method not implemented.');
  }

  fetchUserData() {
    this.service.getLoggedInUser().subscribe(
      (data: any) => {
        console.log('User data:', data);
        if (data) {
          this.customerData = data;
          // User data is available, proceed with other actions if needed
        } else {
          console.warn('User data not available');
          // You may choose to set some default values for userData here
        }
      },
      (error: any) => {
        console.error('Failed to fetch user data:', error);
        this.toastr.error('Failed to fetch user data. Please try again.');
      }
    );
  }


  updateProfile(formData: any) {
    // Call service method to update user profile
    this.service.updateCustomerProfile(formData).subscribe(
      (response: any) => {
        // Handle success response
        this.toastr.success('Profile updated successfully!');
      },
      (error: any) => {
        // Handle error response
        this.toastr.error('Failed to update profile. Please try again.');
      }
    );
  }
}