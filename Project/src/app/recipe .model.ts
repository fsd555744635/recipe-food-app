export interface Recipe {
  id: number;
  name: string;
  chefName: string;
  duration: string;
  category: string;
  base64Image: string;
}
