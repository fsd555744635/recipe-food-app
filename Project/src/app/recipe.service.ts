// recipe.service.ts
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  recipeList: any[]; // Adjust the type based on your Recipe model
  cartItems$: any;

  constructor(private http: HttpClient) {
    this.recipeList = [];
  }

  getAllRecipes(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/getAllRecipes');
  }

  getRecipeById(recipeId: number): Observable<any> {
    return this.http.get<any>(`http://localhost:8080/getRecipeById/${recipeId}`);
  }

  addRecipes(file: File, title: string, description: string, category: string, chefName: string, duration: number): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    formData.append('title', title);
    formData.append('description', description);
    formData.append('category', category);
    formData.append('chefName', chefName);
    formData.append('duration', duration.toString());

    // You may need to set headers based on your backend requirements
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');

    return this.http.post<any>('http://localhost:8080/addRecipe', formData, { headers });
  }

  deleteRecipeById(recipeId: number): Observable<any> {
    return this.http.delete<any>(`http://localhost:8080/deleteRecipeById/${recipeId}`);
  }

  getRecipesByCategory(category: string): Observable<any[]> {
    return this.http.get<any[]>(`http://localhost:8080/getRecipesByCategory/${category}`);
  }

  getRecipesByChefName(chefName: string): Observable<any[]> {
    return this.http.get<any[]>(`http://localhost:8080/getRecipesByChefName/${chefName}`);
  }

  getRecipesWithDurationLessThanOrEqual(maxDuration: number): Observable<any[]> {
    return this.http.get<any[]>(`http://localhost:8080/getRecipesWithDurationLessThanOrEqual/${maxDuration}`);
  }

  // Add other methods as needed
}
