import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RecipeService } from '../recipe.service';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  recipes: any;
  cartRecipes: any[];
  service: any;
  customerService: any;
  constructor(private router: Router, private cartService: CartService) {}


  ngOnInit() {

  }

  navigateToComponent(component: string) {
    if (component === 'vegitem') {
      this.router.navigate(['/vegitem']);
    } else if (component === 'nonvegitem') {
      this.router.navigate(['/non-vegitem']);
    }
  }
  addToCart(recipe: any) {
    this.cartService.addToCart(recipe);
  }
}