import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from '../customer.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formError: string = '';
  customer: any;
  randomNumber: number;
  email: any; 
  phoneNumber: any;
  otp:any;
  
  constructor(private service: CustomerService, private fb: FormBuilder, private router: Router, private toastr: ToastrService) {
    this.customer = {
      customerName:'',
      gender: '',
      email: '',
      phoneNumber: '',
      password: '',
      confirmPassword: '' 
    };
    this.randomNumber = 0;
  }

  ngOnInit() {
    
  }

  showPassword: boolean = false;

  focusPasswordField(passwordField: any) {
    if (passwordField instanceof HTMLInputElement) {
      passwordField.focus();
    }
  }


  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }

  getRandomNumber(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  async registerSubmit(formData: any, form: NgForm) {
    if (
      !formData.customerName ||
      !formData.gender ||
      !formData.email ||
      !formData.phoneNumber||
      !formData.password ||
      !formData.confirmPassword
    ) {
      this.formError = 'All fields are required';
      this.toastr.error('All fields are required');
      return;
    }

    if (formData.password !== formData.confirmPassword) {
      this.formError = 'Passwords do not match';
      form.controls['confirmPassword'].setErrors({ passwordMismatch: true });
      return;
    }

    this.formError = '';

    this.customer = {
      customerName: formData.customerName,
      gender: formData.gender,
      email: formData.email,
      phoneNumber: formData.phoneNumber,
      password: formData.password,
      confirmPassword: formData.confirmPassword,
      otp: 0, 
    };

    console.log(formData);

    this.randomNumber = this.getRandomNumber(100000, 999999);

    await this.service.sendOtpToCustomer(this.customer.phoneNumber, this.randomNumber).toPromise()
      .then((flag: any) => {
        if (flag) {
          this.customer.otp = String(this.randomNumber);
          this.service.customer = this.customer;
          this.service.otp = this.randomNumber;
          this.service.setIsUserLoggedIn();
          this.router.navigate(['otp']);
        } else {
          this.toastr.error('OTP is not sent to the customer');
        }
      })
      .catch((error) => {
        console.error('Error sending OTP:', error);
        this.toastr.error('Error sending OTP. Please try again.');
      });
  }
};