import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-searchcustomers',
  templateUrl: './searchcustomers.component.html',
  styleUrl: './searchcustomers.component.css'
})
export class SearchcustomersComponent implements OnInit {

  customerId:any;
  customer:any;

  constructor(private service : CustomerService){}

  ngOnInit(){}

  getById(){
    this.service.getCustomerById(this.customerId).subscribe((data: any) => {this.customer = data;}) // Corrected parameter
  }
}