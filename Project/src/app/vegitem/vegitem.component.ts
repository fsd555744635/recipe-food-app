import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';
//import { VegItemService } from '../veg-item.service';

@Component({
  selector: 'app-veg-items',
  templateUrl:  './vegitem.component.html',
  styleUrls: ['./vegitem.component.css']
})
export class VegItemComponent{
  vegItems: any[];

  veg = [
    { name: 'Ladies Finger', image: 'assets/Images/ladiesfinger.jpg', description: 'Delicious and nutritious okra curry, perfect for vegetarians. Okra, also known as bhindi or lady', price: 40.00 },
    { name: 'Potato Curry', image: 'assets/Images/potato1.jpg', description: 'Savory potato curry cooked with aromatic spices. Potatoes are boiled and then simmered in a rich and flavorful gravy made with onions, tomatoes, and spices.', price: 35.00 },
    { name: 'Tomato Curry', image: 'assets/Images/tomato.jpg', description: 'Tangy tomato curry cooked with onions, tomatoes, and spices. This curry is bursting with flavors and is a perfect accompaniment to rice or bread.', price: 40.00 },
    { name: 'Vegetable Pulao', image: 'assets/Images/phulava.jpg', description: 'Fragrant rice cooked with mixed vegetables and spices. This aromatic dish is a one-pot meal that is both flavorful and nutritious.', price:35.00 },
    { name: 'Paneer Curry', image: 'assets/Images/panner.jpg', description: 'Creamy paneer curry made with tender paneer cubes marinated in spices and cooked in a rich and creamy tomato-based gravy.', price: 20.00 },
    { name: 'Vegetable Biryani', image: 'assets/Images/vegtablebriyani.jpg', description: 'Flavorful rice dish cooked with assorted vegetables and aromatic spices. This biryani is a perfect blend of flavors and textures, making it a favorite among vegetarians.', price: 50.00 },
    { name: 'Dal Tadka', image: 'assets/Images/daltadka.jpg', description: 'Lentils cooked with spices and tempered with aromatic ghee. This comforting dal is a staple in Indian cuisine and is best enjoyed with rice or bread.', price:120.00  },
    { name: 'Chana Masala', image: 'assets/Images/cholebature.jpg', description: 'Chickpeas cooked in a spicy and tangy tomato-based sauce. This classic Indian dish is hearty, flavorful, and perfect for any occasion.', price:200.00  },
    { name: 'Tomato Rice', image: 'assets/Images/tomatorice.jpg', description: 'Tangy and flavorful rice dish made with cooked rice tossed in a spicy tomato-based masala. This dish is quick to make and bursting with flavors.', price:  100.00},
    { name: 'Mixed Veg Curry', image: 'assets/Images/mixvegetable.JPG', description: 'Assorted vegetables cooked in a flavorful curry sauce. This versatile curry can be made with a variety of vegetables and is perfect for any meal.', price:  100.00},
    { name: 'Curd Rice', image: 'assets/Images/curdrice.jpg', description: 'Comforting rice dish made with cooked rice mixed with yogurt and seasoned with spices. This dish is cool, creamy, and refreshing, making it perfect for hot days.', price:  100.00},
    { name: 'Cabbage Curry', image: 'assets/Images/cabbage1.webp', description: 'Crunchy cabbage cooked with onions, tomatoes, and spices. This simple yet delicious curry is a great way to enjoy the goodness of cabbage.', price:  100.00},
    { name: 'Beans Curry', image: 'assets/Images/beans.jpg', description: 'Green beans cooked with onions, tomatoes, and spices. This flavorful curry is a great way to incorporate vegetables into your diet.', price:  100.00},
    { name: 'Cauliflower Curry', image: 'assets/Images/cauliflower.jpg', description: 'Tender cauliflower florets cooked in a spicy and flavorful curry sauce. This dish is nutritious, delicious, and perfect for vegetarians.', price:  100.00},
    { name: 'Brinjal Curry', image: 'assets/Images/brinjal1.jpg', description: 'Eggplant cooked with onions, tomatoes, and spices. This flavorful curry is a great way to enjoy the unique taste of brinjal.', price:  100.00},
    { name: 'Rotis', image: 'assets/Images/roti.jpg', description: 'Traditional Indian flatbread made with whole wheat flour. These soft and fluffy rotis are perfect for scooping up curries and other dishes.', price:  100.00},
];


  showSuccessMessage: boolean = false;
  constructor(private router: Router, private cartService: CartService) { }

  addToCart(veg: any) {
    this.cartService.addToCart(veg); 
    this.showSuccessMessage = true;
    setTimeout(() => {
      this.showSuccessMessage = false;
    }, 3000);
  }
}
